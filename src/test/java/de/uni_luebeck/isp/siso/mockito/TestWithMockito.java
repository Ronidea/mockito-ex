package de.uni_luebeck.isp.siso.mockito;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.mockito.Mockito.*;
import static org.junit.jupiter.api.Assertions.*;

/**
 * Test suite using Mockito to generate mock objects.
 * 
 * @author marcel pflaeging, roni draether
 *
 */
public class TestWithMockito {
    Provider provider;
    Listener a,b;
    private static final String notification1 = "One";
    private static final String notification2 = "Two";

    @BeforeEach
    public void init() {
        provider = new Provider();
        a = mock(Listener.class);
        b = mock(Listener.class);
    }

    @AfterEach
    public void destruct() {
        provider = null;
        a = null;
        b = null;
    }

    @Test
    public void testNotifyListenersRegisteredOnce() {
        // mock listener
        Listener a = mock(Listener.class);

        // perform action
        provider.addListeners(a);
        provider.notifyListeners(notification2);
        provider.notifyListeners(notification1);
        provider.notifyListeners(notification2);

        // verify number of invocations with specific args
        verify(a, times(1)).notify(notification1);
        verify(a, times(2)).notify(notification2);

    }

    @Test
    public void testNotifyListenersRegisteredTwice() {
        // mock listener

        // perform action
        provider.addListeners(b,b);
        provider.notifyListeners(notification2);
        provider.notifyListeners(notification1);
        provider.notifyListeners(notification2);

        verify(b, times(2)).notify(notification1);
        verify(b, times(4)).notify(notification2);
    }

    @Test
    public void testRemoveListeners() {
        provider.addListeners(a,b);
        provider.removeListener(b);
        provider.notifyListeners(notification1);

        verify(a, times(1)).notify(notification1);
        verify(b, times(0)).notify(any());
    }

    @Test
    public void testClearListeners() {
        provider.addListeners(a,b);
        provider.clearListeners();
        provider.notifyListeners(notification1);

        verify(a, times(0)).notify(notification1);
        verify(b, times(0)).notify(any());
    }

}

package de.uni_luebeck.isp.siso.mockito;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Test suite using a mock class written manually.
 *
 * @author marcel pflaeging, roni draether
 *
 */
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class TestWithManualMockup {
	// TODO Write JUnit test cases

    private static Provider provider;
    private static final String notification1 = "One";
    private static final String notification2 = "Two";

    @BeforeAll
    public void setUp() {
        provider = new Provider();
    }

    /**
     * Tears down the test fixture.
     * (Called after every test case method.)
     */
    @AfterAll
    public void tearDown() {
        provider = null;
    }

    @Test
    public void testNotifyListenersRegisteredOnce() {
        // instantiate mock object
        MockListener a = new MockListener();

        // perform action
        provider.addListeners(a);
        provider.notifyListeners(notification2);
        provider.notifyListeners(notification1);
        provider.notifyListeners(notification2);

        // verify number of invocations with specific args
        assertEquals(1, a.getNotificationCount(notification1));
        assertEquals(2, a.getNotificationCount(notification2));
    }

    @Test
    public void testNotifyListenersRegisteredTwice() {
        // instantiate mock object
        MockListener b = new MockListener();

        // perform action
        provider.addListeners(b,b);
        provider.notifyListeners(notification2);
        provider.notifyListeners(notification1);
        provider.notifyListeners(notification2);

        // verify number of invocations with specific args
        assertEquals(2, b.getNotificationCount(notification1));
        assertEquals(4, b.getNotificationCount(notification2));
    }

    static class MockListener implements Listener {
        private final Map<String, Integer> argCMap = new HashMap<>();

        public int getNotificationCount(String notification) {
            return argCMap.get(notification);
        }

        @Override
        public void notify(Object notification) {
            Integer i = argCMap.get((String) notification);
            argCMap.put((String) notification, (i != null ? i : 0) + 1);
        }
    }
}
